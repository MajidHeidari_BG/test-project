<?php

namespace App\Http\Requests\Api;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\Rule;

class ColumnRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $listID = $this->list_id;
        $title = $this->title;
        return [
            'list_id' => 'required|string|exists:user_lists,_id',
            /*'list_id' => [
                'required',
                'string',
                Rule::exists('user_lists', '_id')->where(function ($query) {
                    return $query->whereHas('columns', function ($column) {
                        dd()
                    });
                })
            ],*/
            'title' => [
                'required',
                Rule::unique('columns')->where(function ($query) use ($listID, $title) {
                    return $query->where('list_id', $listID)->where('title', $title);
                })->ignore($this->id, '_id')
            ],
            'status' => 'required|boolean',
            'sortable' => 'required|boolean',
            'searchable' => 'required|boolean',
            'data_type' => 'required|in:string,integer,float,date,enum',
            'allowed' => 'required_if:data_type,enum|array',
            'allowed.*' => 'string',
            'width_percent' => 'nullable|numeric|between:1,100'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if(Request::is('api/*')) {
            $response = response()->json([
                'message' => 'Invalid data send',
                'details' => $validator->messages(),
            ], 422);

            throw new HttpResponseException($response);
        }
    }
}
