<?php

namespace App\Http\Requests\Api;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Request;

class RecordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'content' => 'required',
            'column_id' => 'required|exists:columns,_id'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if(Request::is('api/*')) {
            $response = response()->json([
                'message' => 'Invalid data send',
                'details' => $validator->messages(),
            ], 422);

            throw new HttpResponseException($response);
        }
    }
}
