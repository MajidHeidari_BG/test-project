<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Request;
use Illuminate\Validation\Rule;

class ListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => [
                'required',
                'string',
                'max:2550',
                Rule::unique('user_lists')->where('title', $this->title)->ignore($this->id, '_id')
            ],
            'meta_data' => 'nullable',
            'per_page' => 'nullable|numeric'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if(Request::is('api/*')) {
            $response = response()->json([
                'message' => 'Invalid data send',
                'details' => $validator->messages(),
            ], 422);

            throw new HttpResponseException($response);
        }
    }
}
