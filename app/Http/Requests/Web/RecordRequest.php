<?php

namespace App\Http\Requests\Web;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Request;

class RecordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'list_id' => 'required|exists:user_lists,_id',
            'record' => 'required|array'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        if(Request::wantsJson()) {
            $response = response()->json([
                'message' => $validator->messages()->first(),
            ], 422);

            throw new HttpResponseException($response);
        }
    }
}
