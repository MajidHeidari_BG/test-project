<?php

namespace App\Http\Requests\Web;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Request;

class ListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|string|max:255',
            'per_page' => 'required|numeric',
            'meta_data' => 'nullable|string',
            'columns' => 'required|array|min:1',
            'columns.*.title' => 'required|string|max:255',
            'columns.*.width_percent' => 'nullable|numeric|between:1,100',
            'columns.*.searchable' => 'required|boolean',
            'columns.*.status' => 'required|boolean',
            'columns.*.sortable' => 'required|boolean',
            'columns.*.data_type' => 'required|in:string,integer,float,dateTime,enum',
        ];
        if ($this->_method == "PATCH") {
            $rules['columns.*.id'] = 'required|exists:columns,_id';
            $rules['columns.*.allowed'] = 'nullable';
        } else {
            $rules['columns.*.allowed'] = 'required_if:columns.*.data_type,enum';
        }
        return $rules;
    }

    /**
     * Return response for validating the request.
     *
     * @return array<string, mixed>
     */
    protected function failedValidation(Validator $validator)
    {
        if(Request::wantsJson()) {
            $response = response()->json([
                'message' => $validator->messages()->first(),
            ], 422);

            throw new HttpResponseException($response);
        }
    }
}
