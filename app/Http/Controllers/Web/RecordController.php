<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\RecordRequest;
use App\Models\UserList;
use App\Repositories\RecordRepository;
use App\Repositories\UserListRepository;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class RecordController extends Controller
{
    private $list;
    public function __construct(UserListRepository $listRepository, RecordRepository $recordRepository)
    {
        $this->list = $listRepository;
        $this->record = $recordRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Create new record.
     *
     * @param RecordRequest $request
     * @return JsonResponse
     */
    public function store(RecordRequest $request)
    {
        $list = $this->list->find($request->list_id, ['columns']);
        $result = $this->customValidation($list, $request);
        if (!$result['validated']) {
            return Response::json([
                'message' => $result['error'],
            ], 422);
        }
        $recordGroupID = $this->record->groupIDGenerator();
        foreach ($result['data'] as $recordData) {
            $recordData['group_id'] = $recordGroupID;
            $record = $this->record->store($recordData);
        }
        return Response::json([
            'message' => 'Your record created successfully.',
        ]);
    }

    /**
     * Create multiple records based on external api.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeApi(Request $request)
    {
        $this->validate($request, [
            'api' => 'required|url',
            'list_id' => 'required|exists:user_lists,_id'
        ]);
        $list = $this->list->find($request->list_id, ['columns']);
        $client = Http::get($request->api);
        $result = $client->json();
        $rules = [];
        foreach ($list->columns as $column) {
            $rules['*.' . str_replace(' ', '_', $column->title)] = 'required';
        }
        $validator = Validator::make($result, $rules);
        if ($validator->fails()) {
            $exp = explode('.', $validator->getMessageBag()->keys()[0]);
            $message = "The field $exp[1] in item " . $exp[0] + 1 . " is required.";
            return redirect()->back()->withErrors([$message]);
        }


        foreach ($result as $record) {
            $groupID = $this->record->groupIDGenerator();
            foreach ($record as $index => $item) {
                $column = $list->columns->where('title', '==', str_replace('_', ' ', $index))->first();
                $recordData = [
                    'group_id' => $groupID,
                    'list_id' => $request->list_id,
                    'column_id' => $column->id,
                    'content' => $item
                ];
                $this->record->store($recordData);
            }
        }
        return redirect()->back()->with('success_message', count($result) . " records created successfully.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Validate request and return array of validated request
     *
     * @param UserList $list
     * @param RecordRequest $request
     * @return array
     *
     */
    private function customValidation($list, $request)
    {
        $result = [];
        foreach ($list->columns as $column) {
            $recordValue = $this->searchForColumn($column->id, $request->record);
            if (is_null($recordValue)) {
                return ['validated' => false, 'error' => "The $column->title field is required."];
            }
            $result[] = [
                'column_id' => $column->id,
                'list_id' => $list->id,
                'content' => $recordValue
            ];
        }
        return ['validated' => true, 'data' => $result];
    }

    function searchForColumn($id, $array) {
        foreach ($array as $key => $val) {
            if ($val['column_id'] == $id) {
                return $array[$key]['column_value'];
            }
        }
        return null;
    }
}
