<?php

namespace App\Http\Controllers\Web;

use App\Helpers\Paginator;
use App\Http\Controllers\Controller;
use App\Http\Requests\Web\ListRequest;
use App\Repositories\ColumnRepository;
use App\Repositories\UserListRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class ListController extends Controller
{
    private $list;
    private $column;
    public function __construct(UserListRepository $listRepository, ColumnRepository $columnRepository)
    {
        $this->list = $listRepository;
        $this->column = $columnRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        $lists = $this->list->getAll();
        return view('lists.index', compact('lists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('lists.create');
    }

    /**
     * Creating new User List with its columns
     *
     * @param ListRequest $request
     * @return JsonResponse
     */
    public function store(ListRequest $request)
    {
        $list = $this->list->store($request->only(['meta_data', 'title', 'per_page']));
        foreach ($request->columns as $column) {
            $column['list_id'] = $list->id;
            $column = $this->column->store($column);
        }
        return Response::json([
            'message' => 'Your list created successfully.',
            'data' => $list->with('columns')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return View
     */
    public function show($id)
    {
        $list = $this->list->find($id, ['columns', 'records' ]);
        $columns = $list->columns->where('status', '1');
        $records = $list->records
            ->whereIn('column_id', $columns->pluck('_id')->toArray())
            ->groupBy('group_id');
        $records = Paginator::paginate($records, $list->per_page)->setPath('/lists/' . $list->id);

        $data = [
            'records' => $records,
            'columns' => $columns,
            'list' => $list
        ];
        return view('lists.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $list = $this->list->find($id, ['columns']);
        return view('lists.edit', compact('list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return JsonResponse
     */
    public function update($id, ListRequest $request)
    {
        $list = $this->list->update($id, $request->only(['meta_data', 'title', 'per_page']));
        foreach ($request->columns as $column) {
            $column = $this->column->update($column['id'], collect($column)->except('id')->toArray());
        }
        return Response::json([
            'message' => 'Your list updated successfully.',
            'data' => $list->with('columns')
        ]);
    }

    /**
     * Remove the specified List with all of its columns and records.
     *
     * @param  $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        $this->list->destroy($id);
        return redirect()->back()->with('success_message', 'Your list deleted successfully.');
    }
}
