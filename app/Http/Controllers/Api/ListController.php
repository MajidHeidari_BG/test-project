<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\ColumnRepository;
use App\Repositories\UserListRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class ListController extends Controller
{
    private $lists;
    private $columns;
    public function __construct(UserListRepository $listRepository, ColumnRepository $columnRepository)
    {
        $this->lists = $listRepository;
        $this->columns = $columnRepository;
    }

    public function lists(Request $request)
    {
        $lists = $this->lists->getAll();
        return Response::json($lists);
    }

    public function records($listID, Request $request)
    {
        $list = $this->lists->find($listID, ['columns']);
        $page = request('page', 1);
        $perPage = $list->per_page;
        $offset = ($page - 1) * $perPage;
        $activeColumns = $list->columns->where('status', '=', '1')->pluck('_id')->toArray();
        $perPage = $perPage * count($activeColumns); // each record contains multiple columns so per page should be per_page * columns count;

        $recordsGroupID = $list->records();

        // search in records by specific columns
        $searched = null;
        if ($request->has('search_by') && strlen(trim($request->search_value)) > 0) {
            $column = $list
                ->columns
                ->where('_id', $request->search_by)
                ->first();
            if ($column != null) {
                $searched = [
                    'column' => $column->id,
                    'search' => $request->search_value ?? ''
                ];
                $columnID = $column->id;
                $recordsGroupID = $recordsGroupID
                    ->where('column_id', $columnID)
                    ->where('content', 'LIKE', "%" . $request->search_value . "%");
            }
        }

        $totalRecords = $recordsGroupID->count();

        // search result records grouped by "group_id"
        $recordsGroupID = $recordsGroupID
            ->skip($offset)
            ->take($perPage)
            ->pluck('group_id')
            ->toArray();

        $recordsGroupID = array_unique($recordsGroupID);

        // get records
        $searchedRecords = $list
            ->records()
            ->whereIn('group_id', $recordsGroupID)
            ->whereIn('column_id', $activeColumns)
            ->get()
            ->groupBy('group_id')
            ->toArray();

        // sort records based on specific column
        $records = [];
        $sortBy = null;
        if ($request->has('sort_by')) {
            $column = $list->columns->where('_id', $request->sort_by)->first();
            if ($column != null) {
                $primaryRecords = null;
                $sortBy = $column->id;
                foreach ($searchedRecords as $index => $item) {
                    $column = array_filter($item, function($ar) use ($sortBy) {
                        return ($ar['column_id'] == $sortBy);
                    });
                    $column = array_values($column);
                    $primaryRecords[$index] = $column[0]['content'];
                }
                if ($primaryRecords != null) {
                    asort($primaryRecords);
                    foreach ($primaryRecords as $index => $record) {
                        $records[$index] = $searchedRecords[$index];
                    }
                }
            }
        }

        return Response::json([
            'data' => count($records) > 0 ? $records : $searchedRecords,
            'sort_by' => $sortBy,
            'searched' => $searched,
            'meta' => [
                'current_page' => $page,
                'per_page' => $perPage,
                'total_pages' => ceil($totalRecords / $perPage),
                'total_records' => $totalRecords / count($activeColumns)
            ]
        ], 200);
    }
}
