<?php

namespace App\Repositories;

use App\Http\Requests\ListRequest;
use App\Models\UserList;

class UserListRepository
{

    /**
     * Return paginate list from UserLists
     *
     * @return array
     */
    public function getAll()
    {
        $totalLists = UserList::count();
        $perPage = request('per_page', 15);
        $page = request('page', 1);
        $offset = ($page - 1) * $perPage;
        $lists = UserList::skip($offset )->take($perPage)->with(['columns'])->get()->makeHidden(['created_at', 'updated_at']);
        return [
            'data' => $lists,
            'meta' => [
                'current_page' => $page,
                'per_page' => $perPage,
                'total_pages' => ceil($totalLists / $perPage),
                'total_items' => $totalLists
            ]
        ];
    }


    /**
     * Create new resource of UserLists
     *
     * @param $data
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function store($data)
    {
        return UserList::create($data);
    }

    public function update($id, $data)
    {
        $list = $this->find($id);
        $list->update($data);
        return $list;
    }

    public function find($id, $relations = [])
    {
        return UserList::with($relations)->findOrFail($id);
    }

    /**
     * Delete UserList with all of its columns and records
     *
     * @param $id
     * @return bool|mixed|null
     */
    public function destroy($id)
    {
        return UserList::with(['records', 'columns'])->findOrFail($id)->delete();
    }
}
