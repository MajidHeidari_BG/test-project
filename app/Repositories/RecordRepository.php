<?php

namespace App\Repositories;

use App\Models\Record;
use Illuminate\Support\Str;

class RecordRepository
{

    public function store(array $data)
    {
        return Record::create($data);
    }

    public function update($id, array $data)
    {
        $record = $this->find($id);
        $record->update($data);
        return $record;
    }

    public function find($id)
    {
        return Record::findOrFail($id);
    }

    /**
     * Generate Random and unique uuid for group of records
     *
     * @return string
     */
    public function groupIDGenerator()
    {
        $groupID = Str::uuid()->toString();
        if (Record::where('group_id', $groupID)->exists()) {
            $this->groupIDGenerator();
        }
        return $groupID;
    }
}
