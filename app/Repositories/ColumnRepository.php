<?php

namespace App\Repositories;

use App\Models\Column;

class ColumnRepository
{

    /**
     * Create new resource of Column
     *
     * @param $data
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function store($data)
    {
        return Column::create($data);
    }

    /**
     * Update resource of Column
     *
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function update($id, array $data)
    {
        $column = $this->find($id);
        if ($data['data_type'] != 'enum') {
            $data['allowed'] = null;
        } else {
            $data['allowed'] = $data['allowed'] != null ? $column->allowed . ',' . $data['allowed'] : $column->allowed ;
        }
        $column->update($data);

        return $column;
    }

    /**
     * Find specific resource of Column
     *
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function find($id)
    {
        return Column::findOrFail($id);
    }

}
