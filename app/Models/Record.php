<?php

namespace App\Models;


use Jenssegers\Mongodb\Eloquent\Model;

class Record extends Model
{

    protected $fillable = [
        'column_id',
        'content',
        'data_type',
        'list_id',
        'group_id'
    ];
}
