<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class UserList extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title',
        'per_page',
        'meta_data'
    ];

    /**
     * @return \Jenssegers\Mongodb\Relations\HasMany
     */
    public function columns()
    {
        return $this->hasMany(Column::class, 'list_id', '_id');
    }

    /**
     * @return \Jenssegers\Mongodb\Relations\HasMany
     */
    public function records()
    {
        return $this->hasMany(Record::class, 'list_id', '_id');
    }

    public function scopeCustomPaginate($query)
    {
        $page = request('page', 1);
        $perPage = request('per_page', 15);
        $offset = $page * $perPage;
        return $query->take($perPage)->skip($offset)->get();
    }

    protected static function boot() {
        parent::boot();

        static::deleting(function($list) {
            $list->records()->delete();
            $list->columns()->delete();
        });
    }
}
