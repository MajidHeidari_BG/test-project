<?php

namespace App\Models;



use Jenssegers\Mongodb\Eloquent\Model;

class Column extends Model
{

    protected $fillable = [
        'list_id',
        'data_type',
        'allowed',
        'width_percent',
        'title',
        'searchable',
        'sortable',
        'status',
    ];


    public function records()
    {
        return $this->hasMany(Record::class, 'column_id', '_id');
    }

    public function scopeActive($query)
    {
        $query->where('status', "1");
    }
}
