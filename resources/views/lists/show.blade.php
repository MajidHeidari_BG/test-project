@extends('layouts.app')

@section('title', $data['list']->title ?? 'list showing')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card mb-5">
                <div class="card-header">
                    <h4 class="card-title" id="list" data-listID="{{ $data['list']->id }}">
                        {{ $data['list']->title ?? 'list showing' }}
                        <a href="{{ route('lists.index') }}" class="btn btn-outline-primary float-right"> back </a>
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row pb-3">
                        <h4>Insert new record</h4>
                        @foreach($data['columns'] as $column)
                        <div class="col-lg-6 col-sm-12 pr-3 pl-3 pb-2">
                            <div class="form-group">
                                <label>{{ $column->title }}</label>
                                @switch($column->data_type)
                                    @case('string') <input type="text" data-column="{{ $column->id }}" class="form-control record-value"> @break
                                    @case('integer') <input type="number" data-column="{{ $column->id }}" class="form-control record-value"> @break
                                    @case('dateTime') <input type="date" data-column="{{ $column->id }}" class="form-control record-value"> @break
                                    @case('float') <input type="number" data-column="{{ $column->id }}" class="form-control record-value"> @break
                                    @case('enum')
                                        <select class="form-control record-value" data-column="{{ $column->id }}">
                                            @foreach(explode(',', $column->allowed) as $option)
                                                <option value="{{ $option }}">{{ $option }}</option>
                                            @endforeach
                                        </select>
                                        @break
                                @endswitch
                            </div>
                        </div>
                        @endforeach
                        <div class="col-12 d-grid">
                            <button id="insert-record" class="btn btn-success"> <i class="fa fa-check"></i> save </button>
                        </div>
                    </div>
                    <hr>
                    <form action="{{ url('records/store/api') }}" method="POST">
                        <div class="row pb-3">
                            <h4>Insert records from external resource</h4>
                            <div class="col-12 pr-3 pl-3 pb-2">
                                <div class="form-group">
                                    <label>Api</label>
                                    <input type="url" name="api" class="form-control">
                                    @csrf
                                    <input type="hidden" name="list_id" value="{{ $data['list']->id }}">
                                </div>
                            </div>
                            <div class="col-12 d-grid">
                                <button  class="btn btn-info"> <i class="fa fa-upload"></i> start </button>
                            </div>
                        </div>
                    </form>
                    <hr>
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            @foreach($data['columns'] as $column)
                                <th width="{{ $column->width_percent  }}%">
                                    {{ $column->title }}
                                    @if($column->sortable)
                                        <button data-column="{{ $column->id }}" data-list="{{ $data['list']->id }}" class=" btn btn-sm btn-icon btn-outline-primary sort-column"><i class="fa fa-sort"></i></button>
                                    @endif
                                    @if($column->searchable)
                                        <input data-column="{{ $column->id }}" ata-list="{{ $data['list']->id }}" type="text" class="form-control search-column" placeholder="search">
                                    @endif
                                </th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        @if($data['records']->count() > 0)
                            @foreach($data['records'] as $groupID => $items)
                                <tr data-group="{{ $groupID }}">
                                    @foreach($items as $item)
                                        <td>{{ $item->content }}</td>
                                    @endforeach
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class="text-center" colspan="{{ $data['columns']->count() }}">
                                    No data available
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>

                    {!! $data['records']->links() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        const list_id = "{{ $data['list']->id }}";
        function getRecords() {
            let sort = localStorage.getItem('sort')
            let page = localStorage.getItem('page')
            let searched = localStorage.getItem('searched')
            $.get({
                url: `/api/lists/${list_id}/records?${sort}&${searched}&${page}`,
                success: function (response) {
                    $('tbody').empty();
                    $.each(response.data, function (key, value) {
                        let tds = '';
                        $.each(value, function () {
                            tds += `<td>${this.content}</td>`
                        });
                        let record =
                            `<tr data-group="${key}">${tds}</tr>`
                        $('tbody').append(record)
                    });

                    $('nav>ul').empty();
                    for (let page = 1; page <= response.meta.total_pages; page++) {
                        let is_active = '';
                        if (page == response.meta.current_page) {
                            is_active = 'active'
                        } else {
                            is_active = ''
                        }
                        let page_item =
                            `<li class="page-item page-num ${is_active}" data-page="${page}"><a class="page-link" href="#!">${page}</a></li>`
                        $('nav>ul').append(page_item);
                    }
                }
            });
        }

        $('#insert-record').click(function () {
            let record = [];
            $('.record-value').each(function () {
                let column_id = $(this).data('column');
                let column_value = $(this).val();
                record.push({
                    'column_id': column_id,
                    'column_value': column_value
                });
            });
            $.post({
                url: "{{ route('records.store') }}",
                data: {
                    _token: "{{ @csrf_token() }}",
                    list_id: "{{ $data['list']->id }}",
                    record: record
                },
                success: function (response) {
                    Swal.fire({
                        icon: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 2500
                    }).then(function () {
                        $('input').val('')
                        $('select').val('')
                    });
                },
                error: function (error) {
                    Swal.fire({
                        icon: 'error',
                        title: error.responseJSON.message,
                        showConfirmButton: false,
                        timer: 2500
                    })
                }
            });
        });

        $(document).on('click', '.sort-column', function () {
            let column_id = $(this).data('column');
            localStorage.setItem('sort', `sort_by=${column_id}`)
            getRecords();
        });

        $(document).on('click', '.page-num', function () {
            let page = $(this).data('page')
            localStorage.setItem('page', `page=${page}`)
            getRecords();
        });

        $(document).on('keyup', '.search-column', function () {
            let value = $(this).val();
            let column_id = $(this).data('column');
            $(document).find('.search-column').each(function () {
                $(this).val('');
            });
            $(this).val(value);
            localStorage.setItem('searched', `search_by=${column_id}&search_value=${value}`)
            getRecords();
        })
    </script>
@endsection
