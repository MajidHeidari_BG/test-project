@extends('layouts.app')

@section('title', $title = 'create list')

@section('header')
    <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
    <style>
        .tag-input {
            border-radius: 4px;
            padding-left: 5px;
            padding-right: 5px;
            background-color: blueviolet;
        }
    </style>
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card mb-5">
                <div class="card-header">
                    <h4 class="card-title">
                        {{ $title }}
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <form>
                                <div class="row">
                                    <div class="col-10 mx-auto">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group mb-3">
                                                    <label for="title">title</label>
                                                    <input type="text" id="title" class="form-control" >
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group mb-3">
                                                    <label for="pagination">per page</label>
                                                    <input type="number" id="pagination" class="form-control" >
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="form-group mb-3">
                                                <label for="meta_data">meta data</label>
                                                <textarea name="meta_data" id="meta_data" cols="30" rows="8" class="form-control"></textarea>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="alert alert-warning">
                                                Note:
                                                <strong>The number of columns cannot be edited. (Column details can be edited)</strong>
                                            </div>
                                            <div class="form-group mb-3">
                                                <label for="columns_count">columns count</label>
                                                <div class="input-group mb-3">
                                                    <input type="number" class="form-control" id="columns_count" name="columns_count">
                                                    <div class="input-group-prepend">
                                                        <button class="btn btn-outline-primary" id="column-generator" type="button">Generate</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="columns"></div>

                                        <div class="col-12 d-grid gap-2 mt-3"><button type="button" class="btn btn-success block" id="save"><i class="fa fa-check"></i> save </button></div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
    <script>
        function create_UUID(){
            var dt = new Date().getTime();
            var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = (dt + Math.random()*16)%16 | 0;
                dt = Math.floor(dt/16);
                return (c=='x' ? r :(r&0x3|0x8)).toString(16);
            });
            return uuid;
        }

        $(document).ready(function () {
            $('#column-generator').click(function () {
                let count = $('#columns_count').val();
                $('#columns').empty();
                for (let i = 1; i <= count; i++) {
                    let uuid = create_UUID()
                    let el =
                        `<div data-uuid="${uuid}" class="column-creation row rounded shadow pt-2 pb-2 mt-3">
                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label>column title</label>
                                <input data-uuid="${uuid}" name="column_title" type="text" class="form-control">
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label>width percent</label>
                                <input data-uuid="${uuid}" name="column_width_percent" type="number" min="1" max="100" class="form-control">
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label>status</label>
                                <select class="form-control" data-uuid="${uuid}" name="column_status">
                                    <option value="1">show</option>
                                    <option value="0">hide</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label>searchable</label>
                                <select class="form-control" data-uuid="${uuid}" name="column_searchable">
                                    <option value="1">searchable</option>
                                    <option value="0">unsearchable</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label>data type</label>
                                <select class="form-control dts" data-uuid="${uuid}" name="column_data_type">
                                    <option value="string">string</option>
                                    <option value="integer">integer</option>
                                    <option value="float">float</option>
                                    <option value="dateTime">date time</option>
                                    <option value="enum">enumerator</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group mb-3">
                                <label>sortable</label>
                                <select class="form-control" data-uuid="${uuid}" name="column_sortable">
                                    <option value="1">sortable</option>
                                    <option value="0">unsortable</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-12 alllowed" data-allowed="1" data-uuid="${uuid}" style="display: none;">
                            <div class="form-group mb-3">
                                <label>allowed values</label>
                                <input data-uuid="${uuid}" name="column_alloweds" class="form-control allowed" type="text" style="width: 100% !important;">
                            </div>
                        </div>

                        <div class="col-12 d-grid gap-2"><button type="button" data-uuid="${uuid}" class="btn btn-danger block column-trash"><i class="fa fa-trash"></i> delete column </button></div>
                    </div>`;
                    $('#columns').append(el);
                }
                $('.allowed').tagsinput({
                    tagClass: 'tag-input'
                });
            });

            $(document).on('click', '.column-trash', function () {
                $('#columns_count').val($('#columns_count').val() - 1);
                let uuid = $(this).data('uuid')
                $(document).find(`div[data-uuid=${uuid}]`).remove();
            });

            $('#save').click(function () {
                let columns = [];
                $(document).find('div.column-creation').each(function () {
                    let uuid = $(this).data('uuid');
                    let title = $(`input[data-uuid=${uuid}][name=column_title]`).val();
                    let width_percent = $(`input[data-uuid=${uuid}][name=column_width_percent]`).val();
                    let status = $(`select[data-uuid=${uuid}][name=column_status]`).val();
                    let searchable = $(`select[data-uuid=${uuid}][name=column_searchable]`).val();
                    let data_type = $(`select[data-uuid=${uuid}][name=column_data_type]`).val();
                    let sortable = $(`select[data-uuid=${uuid}][name=column_sortable]`).val();
                    let allowed = $(`input[data-uuid=${uuid}][name=column_alloweds]`).val();
                    columns.push({
                        'title': title,
                        'width_percent': width_percent,
                        'status': status,
                        'searchable': searchable,
                        'data_type': data_type,
                        'sortable': sortable,
                        'allowed': allowed,
                    });
                });

                $.post({
                    url: "/lists",
                    dataType: "json",
                    data: {
                        _token: $('meta[name=csrf]').attr('content'),
                        columns: columns,
                        title: $('#title').val(),
                        per_page: $('#pagination').val(),
                        meta_data: $('#meta_data').val(),
                    },
                    success: function (response) {
                        Swal.fire({
                            icon: 'success',
                            title: response.message,
                            showConfirmButton: true,
                        }).then(function () {
                            window.location.href = "/lists"
                        });
                    },
                    error: function (error) {
                        Swal.fire({
                            icon: 'error',
                            title: error.responseJSON.message,
                            showConfirmButton: true,
                        })
                    }
                });
            });

            $(document).on('change', '.dts', function () {
                let data_type = $(this).val();
                let uuid = $(this).data('uuid');
                if (data_type == 'enum') {
                    $(`div[data-uuid=${uuid}][data-allowed='1']`).show()
                } else {
                    $(`div[data-uuid=${uuid}][data-allowed='1']`).hide()
                    $(`input[data-uuid=${uuid}][name=column_alloweds]`).val('')
                }
            });
        });

        $('.allowed').tagsinput({
            tagClass: 'tag-input'
        })
    </script>
@endsection
