@extends('layouts.app')

@section('title', $title = 'edit list')

@section('header')
    <link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
    <style>
        .tag-input {
            border-radius: 4px;
            padding-left: 5px;
            padding-right: 5px;
            background-color: blueviolet;
        }
    </style>
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card mb-5">
                <div class="card-header">
                    <h4 class="card-title">
                        {{ $title }}
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <form>
                                <div class="row">
                                    <div class="col-10 mx-auto">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group mb-3">
                                                    <label for="title">title</label>
                                                    <input type="text" id="title" class="form-control" value="{{ $list->title }}">
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group mb-3">
                                                    <label for="pagination">per page</label>
                                                    <input type="number" id="pagination" class="form-control" value="{{ $list->per_page }}">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="form-group mb-3">
                                                <label for="meta_data">meta data</label>
                                                <textarea name="meta_data" id="meta_data" cols="30" rows="8" class="form-control">{!! $list->meta_data !!}</textarea>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="alert alert-warning">
                                                Note:
                                                <strong>The number of columns cannot be edited. (Column details can be edited)</strong>
                                            </div>
                                        </div>

                                        <div id="columns">
                                            @foreach($list->columns as $column)
                                                <div data-uuid="{{ $column->id }}" class="column-creation row rounded shadow pt-2 pb-2 mt-3">
                                                    <input data-uuid="{{ $column->id }}" type="hidden" name="column_id" value="{{ $column->id }}">
                                                    <div class="col-6">
                                                        <div class="form-group mb-3">
                                                            <label>column title</label>
                                                            <input data-uuid="{{ $column->id }}" name="column_title" type="text" class="form-control" value="{{ $column->title }}">
                                                        </div>
                                                    </div>

                                                    <div class="col-6">
                                                        <div class="form-group mb-3">
                                                            <label>width percent</label>
                                                            <input data-uuid="{{ $column->id }}" name="column_width_percent" type="number" min="1" max="100" class="form-control" value="{{ $column->width_percent }}">
                                                        </div>
                                                    </div>

                                                    <div class="col-6">
                                                        <div class="form-group mb-3">
                                                            <label>status</label>
                                                            <select class="form-control" data-uuid="{{ $column->id }}" name="column_status">
                                                                <option {{ $column->status == '1' ? 'selected' : '' }} value="1">show</option>
                                                                <option {{ $column->status == '0' ? 'selected' : '' }} value="0">hide</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-6">
                                                        <div class="form-group mb-3">
                                                            <label>searchable</label>
                                                            <select class="form-control" data-uuid="{{ $column->id }}" name="column_searchable">
                                                                <option {{ $column->searchable == '1' ? 'selected' : '' }} value="1">searchable</option>
                                                                <option {{ $column->searchable == '0' ? 'selected' : '' }} value="0">unsearchable</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-6">
                                                        <div class="form-group mb-3">
                                                            <label>data type</label>
                                                            <select disabled class="form-control dts" data-uuid="{{ $column->id }}" name="column_data_type">
                                                                <option {{ $column->data_type == 'string' ? 'selected' : '' }} value="string">string</option>
                                                                <option {{ $column->data_type == 'integer' ? 'selected' : '' }} value="integer">integer</option>
                                                                <option {{ $column->data_type == 'float' ? 'selected' : '' }} value="float">float</option>
                                                                <option {{ $column->data_type == 'dateTime' ? 'selected' : '' }} value="dateTime">date time</option>
                                                                <option {{ $column->data_type == 'enum' ? 'selected' : '' }} value="enum">enumerator</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-6">
                                                        <div class="form-group mb-3">
                                                            <label>sortable</label>
                                                            <select class="form-control" data-uuid="{{ $column->id }}" name="column_sortable">
                                                                <option {{ $column->sortable == '1' ? 'selected' : '' }} value="1">sortable</option>
                                                                <option {{ $column->sortable == '0' ? 'selected' : '' }} value="0">unsortable</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    @if($column->data_type == 'enum')
                                                        <div class="col-12 alllowed" data-allowed="1" data-uuid="{{ $column->id }}">
                                                            <p class="d-inline mb-0">allowed values: </p>
                                                            @foreach(explode(',', $column->allowed) as $allowedItem)
                                                                <span class="tag-input text-white">{{ $allowedItem }}</span>
                                                            @endforeach
                                                            <div class="form-group mt-2 mb-3">
                                                                <label>more allowed values</label>
                                                                <input data-uuid="{{ $column->id }}" name="column_alloweds" class="form-control allowed disabled" type="text" style="width: 100% !important;">
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                            @endforeach
                                        </div>

                                        <div class="col-12 d-grid gap-2 mt-3"><button data-list="{{ $list->id }}" type="button" class="btn btn-success block" id="save"><i class="fa fa-check"></i> save </button></div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('script')
    <script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

    <script>
        $('.allowed').tagsinput({
            tagClass: 'tag-input'
        });

        $('#save').click(function () {
            let columns = [];
            let list_id = $(this).data('list')
            $(document).find('div.column-creation').each(function () {
                let uuid = $(this).data('uuid');
                let title = $(`input[data-uuid=${uuid}][name=column_title]`).val();
                let id = $(`input[data-uuid=${uuid}][name=column_id]`).val();
                let width_percent = $(`input[data-uuid=${uuid}][name=column_width_percent]`).val();
                let status = $(`select[data-uuid=${uuid}][name=column_status]`).val();
                let searchable = $(`select[data-uuid=${uuid}][name=column_searchable]`).val();
                let data_type = $(`select[data-uuid=${uuid}][name=column_data_type]`).val();
                let sortable = $(`select[data-uuid=${uuid}][name=column_sortable]`).val();
                let allowed = $(`input[data-uuid=${uuid}][name=column_alloweds]`).val();
                columns.push({
                    'id': id,
                    'title': title,
                    'width_percent': width_percent,
                    'status': status,
                    'searchable': searchable,
                    'data_type': data_type,
                    'sortable': sortable,
                    'allowed': allowed,
                });
            });

            $.post({
                url: `/lists/${list_id}`,
                dataType: "json",
                data: {
                    _token: $('meta[name=csrf]').attr('content'),
                    _method: "PATCH",
                    columns: columns,
                    title: $('#title').val(),
                    per_page: $('#pagination').val(),
                    meta_data: $('#meta_data').val(),
                },
                success: function (response) {
                    Swal.fire({
                        icon: 'success',
                        title: response.message,
                        showConfirmButton: true,
                    }).then(function () {
                        window.location.href = "/lists"
                    });
                },
                error: function (error) {
                    Swal.fire({
                        icon: 'error',
                        title: error.responseJSON.message,
                        showConfirmButton: true,
                    })
                }
            });
        });
    </script>
@endsection
