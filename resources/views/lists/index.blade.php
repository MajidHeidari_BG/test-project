@extends('layouts.app')

@section('title', $title = 'lists')

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">
                        {{ $title }}
                        <a href="{{ route('lists.create') }}" class="btn btn-outline-primary float-right">
                            <i class="fa fa-plus"></i> create list
                        </a>
                    </h4>

                </div>
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>title</th>
                            <th>columns</th>
                            <th>action</th>
                        </tr>
                        </thead>
                        <tbody>
{{--                        @foreach($lists as $list)--}}
                            {{--<tr>
                                <td width="60%"><strong>{{ $list->title }}</strong></td>
                                <td width="30%">
                                    @foreach($list->columns as $column)
                                        <div >
                                            <p><strong class="btn btn-sm mb-1 btn-disabled btn-success">{{ $column->title }}</strong></p>
                                        </div>
                                    @endforeach
                                </td>
                                <td>
                                    <a href="{{ route('lists.show', ['list' => $list->id]) }}" class="btn btn-primary mb-1 btn-sm">Show</a>
                                    <button onclick="confirmDestroy('destroy-{{ $list->id }}')" class="btn btn-danger mb-1 btn-sm">Delete</button>
                                    <form method="POST" action="{{ route('lists.destroy', ['list' => $list->id]) }}" style="display: none;" id="destroy-{{ $list->id }}"> @method('DELETE') @csrf </form>
                                    <a href="{{ route('lists.edit', ['list' => $list->id]) }}" class="btn btn-info btn-sm">Update</a>
                                </td>
                            </tr>--}}
{{--                        @endforeach--}}
                        </tbody>
                    </table>
                    <hr>
                    <nav id="pagination" aria-label="Page navigation example">
                        <ul class="pagination" id="pages">
                            <li class="page-item page-num"><a class="page-link" href="#">1</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        // destroy list
        function confirmDestroy(id) {
            Swal.fire({
                icon: 'warning',
                title: "Are sure about delete list?!",
                showDenyButton: true,
                showCancelButton: false,
                confirmButtonText: 'Delete',
                denyButtonText: `Don't delete`,
            }).then((result) => {
                if (result.isConfirmed) {
                    $(`#${id}`).submit();
                }
            })
        }

        // render lists table
        function renderTable (page = 1, per_page = 15) {
            $.get({
                url: `/api/lists?per_page=${per_page}&page=${page}`,
                success: function (response) {
                    // render list table records
                    $('tbody').empty();
                    $.each(response.data, function () {
                        let columns = '';
                        $.each(this.columns, function () {
                            columns +=
                                `<div >
                                    <p><strong class="btn btn-sm mb-1 btn-disabled btn-success">${this.title}</strong></p>
                                </div>`
                        })
                        let csrf = $('meta[name=csrf]').attr('content')
                        let tr =
                            `<tr>
                                <td width="60%"><strong>${this.title}</strong></td>
                                <td width="30%">
                                    ${columns}
                                </td>
                                <td>
                                    <a href="/lists/${this._id}" class="btn btn-primary mb-1 btn-sm">Show</a>
                                    <button onclick="confirmDestroy('destroy-${this._id}')" class="btn btn-danger mb-1 btn-sm">Delete</button>
                                    <form method="POST" action="/lists/${this._id}" style="display: none;" id="destroy-${this._id}">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="${csrf}">
                                    </form>
                                    <a href="/lists/${this._id}/edit" class="btn btn-info btn-sm">Update</a>
                                </td>
                            </tr>`
                        $(tr).appendTo('tbody');
                    });

                    // render list table pagination
                    $(document).find('.page-num').remove();
                    for (let page = 1; page <= response.meta.total_pages; page++) {
                        let is_active = '';
                        if (page == response.meta.current_page) {
                            is_active = 'active'
                        } else {
                            is_active = ''
                        }
                        let page_item =
                            `<li class="page-item page-num ${is_active}" data-page="${page}"><a class="page-link" href="#!">${page}</a></li>`
                        $('#pages').append(page_item);
                    }
                }
            });
        }
        $(document).ready(function () {
            renderTable(1, 15);
            $(document).on('click', '.page-num', function () {
                renderTable($(this).data('page'), 15);
            })
        });
    </script>
@endsection
