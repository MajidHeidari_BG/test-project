<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/generator', function () {
    $users = \Database\Factories\UserFactory::times(request('count', 10))->make();
    return response()->json($users);
});
Route::prefix('lists')->group(function () {
    Route::get('/', [\App\Http\Controllers\Api\ListController::class, 'lists']);
    Route::get('/{listID}/records', [\App\Http\Controllers\Api\ListController::class, 'records']);
});
