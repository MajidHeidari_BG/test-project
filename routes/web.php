<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect()->route('lists.index');
});
Route::resource('lists', \App\Http\Controllers\Web\ListController::class);
Route::resource('records', \App\Http\Controllers\Web\RecordController::class);
Route::post('records/store/api', [\App\Http\Controllers\Web\RecordController::class, 'storeApi']);
