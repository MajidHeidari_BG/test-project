<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('columns', function (Blueprint $table) {
            $table->id();
            $table->foreignId('list_id')->constrained('user_lists')->onDelete('cascade');
            $table->enum('data_type', ['string', 'integer', 'float', 'dateTime', 'enum']);
            $table->text('allowed')->nullable()->comment('use when data type is "enum"');
            $table->string('width_percent')->nullable();
            $table->string('title')->nullable();
            $table->integer('searchable')->default(false);
            $table->integer('sortable')->default(false);
            $table->integer('status')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('columns');
    }
};
